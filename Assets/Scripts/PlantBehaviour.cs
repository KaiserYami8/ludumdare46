﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlantBehaviour : MonoBehaviour
{
    public Plant plant;

    public bool isASprout;
    public bool isBenched;

    public float resistance;
    public float waterLevel;
    public float sunGauge;
    public float pestQuantity;

    public float health = 100;
    public bool dead = false;

    public float sunPower;

    [Header("Ref")]

    public SpriteRenderer sprite;
    public SpriteRenderer shadow;

    public Animator feedbackTemp;
    public Animator feedbackWater;
    public Animator feedbackPest;
    public ParticleSystem feedbackDust;

    public Image dirtGauge;
    public Image waterGauge;

    Transform sun;

    [HideInInspector]
    public float alpha;

    void Start()
    {
        sun = GameObject.FindGameObjectWithTag("Sun").transform;

        alpha = 1f;

        if (isASprout)
        {
            //plant = GameManager._instance.plantList[Random.Range(0, GameManager._instance.plantList.Length)];
            plant = transform.parent.GetComponent<Customer>().hisPlant;
            isBenched = true;
        }
        else
        {
            StartCoroutine("checkPest");
        }
        waterLevel = plant.waterNeeded;
        sunGauge = plant.lightNeeded;

    }

    void Update()
    {
        float depletion = Time.deltaTime * .01f;

        waterLevel -= depletion * plant.speedDrying;
        resistance = Mathf.Clamp(resistance - depletion * 50, 0, 100);

        waterGauge.fillAmount = waterLevel / plant.waterNeeded;
        dirtGauge.fillAmount = waterLevel / plant.waterNeeded - 1;

        if (!isASprout)
        {
            sunPower = Mathf.Clamp(Vector2.Distance(transform.position, sun.position), 0, 8) / 8;
            sunPower = Mathf.Abs(sunPower - 1);

            if (sunPower > plant.lightNeeded * 1.2f)
            {
                sunGauge += depletion * plant.heatWeakness;
            }
            else if (sunPower < plant.lightNeeded * .8f)
            {
                sunGauge -= depletion * plant.coldWeakness;
            }
            else
            {
                sunGauge += (sunPower - plant.lightNeeded) * depletion;
            }

            sunGauge = Mathf.Clamp(sunGauge, -1, 1);

            feedbackTemp.SetFloat("Temp", sunGauge - plant.lightNeeded);
        }

        if (waterLevel < plant.waterNeeded * .2f || waterLevel > plant.waterNeeded * 1.8f)
            health -= Time.deltaTime * (2 - resistance / 50);
        if (Mathf.Abs(sunGauge - plant.lightNeeded) > .5f)
            health -= Time.deltaTime * (2 - resistance / 50);
        if (pestQuantity > .1f)
            health -= Time.deltaTime * (2 - resistance / 50);

        if (isASprout)
            health -= Time.deltaTime * (2 - resistance / 50) * .5f;

        if (waterLevel < plant.waterNeeded * 1.2f && waterLevel > plant.waterNeeded * 0.8f)
            health += Time.deltaTime * (resistance / 200);
        if (Mathf.Abs(sunGauge - plant.lightNeeded) < .1f)
            health += Time.deltaTime * (resistance / 200);
        if (pestQuantity == 0f)
            health += Time.deltaTime * (resistance / 200);

        health = Mathf.Clamp(health, 0, 100);

        if (health < 40)
            sprite.GetComponent<Animator>().SetBool("IsDry", true);
        else
            sprite.GetComponent<Animator>().SetBool("IsDry", false);

        if (waterLevel < plant.waterNeeded * .2f)
            feedbackWater.SetBool("Water", true);
        else
            feedbackWater.SetBool("Water", false);
        if (waterLevel > plant.waterNeeded * 1.8f)
            feedbackWater.SetBool("Drown", true);
        else
            feedbackWater.SetBool("Drown", false);


        if (pestQuantity > .1f)
            feedbackPest.SetBool("Pest", true);
        else
            feedbackPest.SetBool("Pest", false);

        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);

        //PLANT DEATH
        if (health == 0 && !GameManager._instance.isTuto && !dead)
        {
            GameManager._instance.QuitSubScene();

            feedbackDust.Play();

            sprite.GetComponent<Animator>().SetBool("Burnt", true);
            dead = true;

            foreach (Customer customer in GameObject.FindObjectsOfType<Customer>())
            {
                if (customer.hisPlant == plant && customer.isWaiting)
                {
                    customer.timeOut = 0;
                    customer.plantTriggered = GetComponent<PlantBehaviour>();

                    StartCoroutine(Death(customer));

                    break;
                }
            }

            if (isASprout)
            {
                transform.parent.GetComponent<Customer>().plantTriggered = GetComponent<PlantBehaviour>();
                transform.parent.GetComponent<Customer>().Triggered();
            }
        }
    }

    IEnumerator Death(Customer customer)
    {
        yield return new WaitForSeconds(2f);
        customer.Triggered();
    }



    IEnumerator checkPest()
    {
        if (Random.value < .05f)
            pestQuantity += .01f;

        if (Random.value < pestQuantity)
            pestQuantity *= 1.05f;

        foreach (PlantBehaviour pt in GameObject.FindObjectsOfType<PlantBehaviour>())
        {
            if (Vector2.Distance(transform.position, pt.transform.position) < 4)
                if (Random.value < pt.pestQuantity)
                    pestQuantity += .01f;
        }

        pestQuantity = Mathf.Clamp(pestQuantity, 0, 2);

        yield return new WaitForSeconds(2f);
        StartCoroutine("checkPest");
    }
}