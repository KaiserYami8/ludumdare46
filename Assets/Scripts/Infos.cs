﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Infos : MonoBehaviour
{
    public SpriteRenderer pot;
    public Image toolToUse;
    public Sprite spray, water;

    public Text infos;

    GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager._instance;
        var pf = gameManager.plantFocused;

        pot.sprite = pf.sprite.sprite;

        infos.text = pf.plant.plantName + "\n\n";

        if (pf.waterLevel < pf.plant.waterNeeded * .2f || pf.waterLevel > pf.plant.waterNeeded * 1.8f)
            infos.text += "Your plant is thirty.\n";
        else if (pf.waterLevel < pf.plant.waterNeeded * 1.2f && pf.waterLevel > pf.plant.waterNeeded * 0.8f)
            infos.text += "Your plant is well hydrated.\n";
        else
            infos.text += "Your plant needs water.\n";

        infos.text += "\n";

        if (pf.sunPower - pf.plant.lightNeeded > .5f)
            infos.text += "Your plant need has too much sunlight\n";
        else if (pf.sunPower - pf.plant.lightNeeded > .1f)
            infos.text += "Your plant need less sunlight\n";

        if (pf.sunPower - pf.plant.lightNeeded < -.5f)
            infos.text += "Your plant need a lot more sunlight\n";
        else if (pf.sunPower - pf.plant.lightNeeded < -.1f)
            infos.text += "Your plant need a more sunlight\n";

        if (Mathf.Abs(pf.sunPower - pf.plant.lightNeeded) < .1f)
            infos.text += "Your plant has enough sunlight\n";

        infos.text += "\n";

        if (pf.pestQuantity > .1f)
            infos.text += "Your plant is infested.\n";
        else if (pf.pestQuantity > 0f)
            infos.text += "Your plant start to get infested.\n";

        if (pf.pestQuantity == 0f)
            infos.text += "Your plant is healthy.\n";

        infos.text += "\n";

        if (pf.resistance > 80)
            infos.text += "Your soil is exceptional.\n";
        else if (pf.resistance > 60)
            infos.text += "Your soil is good.\n";
        else if (pf.resistance > 30)
            infos.text += "Your soil is poor.\n";
        else
            infos.text += "Your soil is filthy.\n";

        infos.text += "\n";

        if (pf.plant.needSpray)
        {
            toolToUse.sprite = spray;
            infos.text += "Your plant need a spray to be hydrated.\n";
        }
        else
        {
            toolToUse.sprite = water;
            infos.text += "Your plant need a watering can to be hydrated.\n";
        }
    }

    public void Validation()
    {
        GameManager._instance.QuitSubScene();
    }
}