﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Watering : MonoBehaviour
{
    public SpriteRenderer pot;

    public Animator dropplets;

    public Animator plicploc;

    private void Start()
    {
        pot.sprite = GameManager._instance.plantFocused.sprite.sprite;
    }

    private void Update()
    {
        if (GameManager._instance.plantFocused.waterLevel >= GameManager._instance.plantFocused.plant.waterNeeded)
            dropplets.SetBool("Drops", true);
        else
            dropplets.SetBool("Drops", false);


        if (!Input.GetMouseButton(0))
        {
            plicploc.SetBool("PlicPloc", false);

            Quaternion lerp = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, -15), .1f);
            transform.rotation = lerp;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButton(0))
        {
            if (!GameManager._instance.plantFocused.plant.needSpray)
                GameManager._instance.plantFocused.resistance -= Time.deltaTime;

            GameManager._instance.plantFocused.waterLevel += Time.deltaTime;
            plicploc.SetBool("PlicPloc", true);

            Quaternion lerp = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, 0, 10), .1f);
            transform.rotation = lerp;
        }
    }

    public void Validation()
    {
        FindObjectOfType<AudioManager>().PlaySound("Pop02");
        GameManager._instance.QuitSubScene();
    }
}