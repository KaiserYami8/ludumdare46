﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Drag : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    private bool isBeingHeld = false;

    public LayerMask layerMask;

    public GroundDetection groundDetection;

    GameManager gameManager;

    int relDepth;

    Customer customer;

    private void Start()
    {
        gameManager = GameManager._instance;
    }

    void Update()
    {
        if (isBeingHeld)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));

            transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, 0);

            if (groundDetection.isOverGround)
                GetComponent<PlantBehaviour>().alpha = 1f;
            else
                GetComponent<PlantBehaviour>().alpha = .5f;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameManager.QuitSubScene();
        }

        /*
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));

            Vector3 worldDir = new Vector3(mousePos.x, mousePos.y, 0) - Camera.main.transform.position;

            RaycastHit hit;

            // Does the ray intersect any objects excluding the player layer
            if (Physics.Raycast(Camera.main.transform.position, worldDir, out hit, Mathf.Infinity, layerMask))
            {
                Debug.DrawRay(Camera.main.transform.position, worldDir * hit.distance, Color.yellow, 1000);
                Debug.Log("Did Hit");
            }
            else
            {
                Debug.DrawRay(Camera.main.transform.position, worldDir * 100, Color.red, 1000);
                Debug.Log("Miss");
            }
        }
        */


        if (groundDetection.ground != null)
        {
            GetComponent<PlantBehaviour>().sprite.sortingOrder = groundDetection.ground.GetComponent<SetLayerOrder>().orderInLayer + relDepth;
            GetComponent<PlantBehaviour>().shadow.sortingOrder = groundDetection.ground.GetComponent<SetLayerOrder>().orderInLayer + relDepth - 1;
            GetComponent<PlantBehaviour>().isBenched = false;
            //relDepth = 0;
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && !gameManager.onFocus)
        {
            if (transform.parent != null)
            {
                customer = transform.parent.GetComponent<Customer>();
                transform.parent = null;
                groundDetection.lastPosition = transform.position;
            }

            gameManager.plantFocused = GetComponent<PlantBehaviour>();

            FindObjectOfType<AudioManager>().PlaySound("Pop");

            switch (gameManager.activeTool)
            {
                case GameManager.ToolEnum.Hand:
                    Vector3 mousePos;
                    mousePos = Input.mousePosition;
                    mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));

                    startPosX = mousePos.x - transform.localPosition.x;
                    startPosY = mousePos.y - transform.localPosition.y;

                    isBeingHeld = true;
                    break;

                case GameManager.ToolEnum.Shovel:
                    if (!GetComponent<PlantBehaviour>().isBenched)
                    {
                        LoadScene("DirtPouring");
                        gameManager.tool.GetComponent<SpriteRenderer>().enabled = false;
                    }
                    break;

                case GameManager.ToolEnum.Water:
                    if (!GetComponent<PlantBehaviour>().isBenched)
                    {
                        LoadScene("Watering");
                        gameManager.tool.GetComponent<SpriteRenderer>().enabled = false;
                    }
                    break;

                case GameManager.ToolEnum.Infos:
                    if (!GetComponent<PlantBehaviour>().isBenched)
                    {
                        LoadScene("Infos");
                        gameManager.tool.GetComponent<SpriteRenderer>().enabled = false;
                    }
                    break;

                case GameManager.ToolEnum.Brush:
                    if (!GetComponent<PlantBehaviour>().isBenched)
                    {
                        LoadScene("Unpesting");
                    }
                    break;

                default:
                    break;
            }
        }
    }

    private void OnMouseUp()
    {
        isBeingHeld = false;

        GetComponent<PlantBehaviour>().alpha = 1f;
        if (!GetComponent<PlantBehaviour>().isBenched)
        {
            if (!groundDetection.isOverGround)
            {
                transform.position = groundDetection.lastPosition;
            }

            FindObjectOfType<AudioManager>().PlaySound("Cloc02");

            if (GetComponent<PlantBehaviour>().isASprout)
            {
                transform.localScale = Vector3.one;

                customer.isOut = true;
                customer.isWaiting = true;
                customer.silhouette.SetBool("Waiting", true);

                customer.plantSprite.transform.parent.gameObject.SetActive(true);

                if (gameManager.isTuto)
                {
                    GameObject.FindGameObjectWithTag("Arrow").transform.GetChild(0).gameObject.SetActive(true);
                    GameObject.FindGameObjectWithTag("DragDrop").gameObject.SetActive(false);
                }
            }
        }
        else
        {
            if (GetComponent<PlantBehaviour>().isASprout)
            {
                transform.position = groundDetection.lastPosition;
            }
        }
    }

    void LoadScene(string sceneName)
    {
        if (SceneManager.sceneCount > 1)
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(1));
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);

        gameManager.onFocus = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Plant"))
        {
            if (other.GetComponent<Drag>().groundDetection.transform.position.y > groundDetection.transform.position.y)
                relDepth = 2;
            else
                relDepth = 0;
        }
    }
}