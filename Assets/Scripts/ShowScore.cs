﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScore : MonoBehaviour
{
    public string text;

    public bool bestScore;

    void Start()
    {
        
    }

    void Update()
    {
        if (bestScore)
            GetComponent<Text>().text = text + Score.instance.bestScore;
        else
            GetComponent<Text>().text = text + Score.instance.score;
    }
}