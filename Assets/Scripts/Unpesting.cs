﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unpesting : MonoBehaviour
{
    public GameObject pest;

    public GameObject insectGroup;

    public SpriteRenderer plant;

    void Start()
    {
        //int count = Mathf.RoundToInt(GameManager._instance.plantFocused.pestQuantity * 10);
        /*int count = 10;

        for (int i = 0; i < count; i++)
        {
            float x = Random.value - .5f;
            float y = Random.value - .5f;

            Instantiate(pest, new Vector3(x, y, 0) * 5, Quaternion.Euler(0, 0, Random.value * 360), insectGroup.transform);
        }

        GameManager._instance.plantFocused.pestQuantity = count / 10;*/

        plant.sprite = GameManager._instance.plantFocused.sprite.sprite;
    }

    void Update()
    {
        if (GameManager._instance.plantFocused.pestQuantity * 10 - insectGroup.transform.childCount > 0)
        {
            float x = Random.value - .5f;
            float y = Random.value - .5f;
            Instantiate(pest, new Vector3(x, y, 0) * 5, Quaternion.Euler(0, 0, Random.value * 360), insectGroup.transform);
        }
    }

    public void Validation()
    {
        FindObjectOfType<AudioManager>().PlaySound("Pop02");
        GameManager._instance.QuitSubScene();
    }
}
