﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tool : MonoBehaviour
{
    public GameManager.ToolEnum tool;
    public Sprite toolSprite;

    GameManager gameManager;


    void Start()
    {
        gameManager = GameManager._instance;
    }

    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (!gameManager.onFocus)
        {
            gameManager.tool.GetComponent<SpriteRenderer>().sprite = toolSprite;
            gameManager.activeTool = tool;

            foreach (Tool t in GameObject.FindObjectsOfType<Tool>())
                t.GetComponent<Animator>().SetBool("Active", true);

            GetComponent<Animator>().SetBool("Active", false);

            if (tool == GameManager.ToolEnum.Shovel && GameManager._instance.isTuto)
            {
                GameObject.FindGameObjectWithTag("Arrow").transform.position = GameObject.FindObjectOfType<PlantBehaviour>().transform.position;
            }
        }
    }
}