﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class Customer : MonoBehaviour
{
    public Plant hisPlant;

    public SpriteRenderer plantSprite;
    SpriteRenderer plantSpriteParent;

    public Animator silhouette;

    public SpriteMask plantMask;

    public bool isOut;
    public bool isWaiting;

    public float timeOut;

    public PlantBehaviour plantTriggered;

    public ParticleSystem psHappy;
    public ParticleSystem psNeutral;
    public ParticleSystem psSad;

    private void Start()
    {
        plantMask.sprite = hisPlant.sprite;

        transform.DOLocalMoveX(0, 1);

        timeOut = 30 + 30 * ((GameManager._instance.nextSprout + 1) % 3);
        //timeOut /= 10;

        isOut = isWaiting = false;

        plantSpriteParent = plantSprite.transform.parent.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (isOut)
        {
            timeOut -= Time.deltaTime;
            if (timeOut <= 0)
            {
                isOut = false;
                silhouette.SetBool("Waiting", false);
            }
        }

        //If plant released on customer
        if (Input.GetMouseButtonUp(0) && plantTriggered != null)
        {
            if (plantTriggered.plant == hisPlant)
            {
                Triggered();
            }
        }

        if (transform.position.x <= -18)
        {
            //Reset customer emplacement
            transform.parent.gameObject.SetActive(false);

            //If Good Health Increase Score
            if (plantTriggered.health > 40)
            {
                Score.instance.score += 2;
                //Score.instance.score += plantTriggered.health;
            }
            else if (plantTriggered.health > 0)
            {
                Score.instance.score += 1;
                //Score.instance.score += plantTriggered.health;
            }

            //If DEAD damage and test if GameOver
            if (plantTriggered.dead)
            {
                FindObjectOfType<AudioManager>().PlaySound("Glass");

                int brokenHearts = 0;
                foreach (GameObject heart in GameObject.FindGameObjectsWithTag("Heart"))
                {
                    if (heart.GetComponent<Animator>().GetBool("Broken"))
                    {
                        brokenHearts++;
                    }
                    else
                    {
                        heart.GetComponent<Animator>().SetBool("Broken", true);
                        brokenHearts++;
                        break;
                    }
                }
                Debug.LogError(brokenHearts);
                if (brokenHearts == 3)
                {
                    GameManager._instance.GameOver();
                }
                else
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }

        if (isOut)
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, .5f);
            plantSpriteParent.color = new Color(plantSpriteParent.color.r, plantSpriteParent.color.g, plantSpriteParent.color.b, .5f);
        }
        else
        {
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            plantSpriteParent.color = new Color(plantSpriteParent.color.r, plantSpriteParent.color.g, plantSpriteParent.color.b, 1);
        }
    }

    public void Triggered()
    {
        Destroy(plantTriggered.gameObject);

        plantTriggered.transform.parent = transform;

        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOLocalMoveX(0, 2));
        mySequence.Append(transform.DOLocalMoveX(-10, 2));

        //transform.DOLocalMoveX(-10, 2);

        plantSpriteParent.gameObject.SetActive(true);

        silhouette.SetBool("Waiting", false);
        if (plantTriggered.health < 40)
        {
            silhouette.SetBool("Sad", true);
            plantSprite.sprite = hisPlant.dry;
            if (!plantTriggered.dead)
                psNeutral.Play();
        }
        if (plantTriggered.health >= 40)
        {
            plantSprite.sprite = hisPlant.sprite;
            psHappy.Play();
        }
        if (plantTriggered.dead)
        {
            plantSprite.sprite = hisPlant.dead;
            psSad.Play();
        }
    }
    private IEnumerator WaitSceneLoad()
    {
        yield return new WaitForSecondsRealtime(2f);
        SceneManager.LoadScene("GameOver");
        Debug.LogError("GameOver");
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Plant"))
        {
            if (!other.GetComponent<PlantBehaviour>().isBenched && !other.GetComponent<PlantBehaviour>().isASprout && !isOut)
            {
                plantTriggered = other.GetComponent<PlantBehaviour>();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        plantTriggered = null;
    }
}