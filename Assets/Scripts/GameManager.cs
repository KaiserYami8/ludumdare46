﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int lvl;
    public float dayTime;

    public float baseTimeScale;

    public enum ToolEnum { Hand, Shovel, Water, Infos, Brush}

    public ToolEnum activeTool;

    public static GameManager _instance;

    public bool onFocus = false;
    public PlantBehaviour plantFocused;

    public GameObject tool;

    public Plant[] plantList;

    public GameObject prefabCustomer;

    public GameObject[] sprouts;
    public int nextSprout = 0;

    public GameObject[] customerSlots;

    public bool isTuto = true;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        //DontDestroyOnLoad(gameObject);


        Score.instance.score = 0;
    }

    private void Start()
    {
        foreach (GameObject slot in customerSlots)
        {
            if (!slot.activeSelf)
            {
                slot.SetActive(true);
                var customer = Instantiate(prefabCustomer, slot.transform.position - new Vector3(5, 0, 0), Quaternion.identity, slot.transform);
                customer.GetComponent<Customer>().hisPlant = plantList[Random.Range(0, GameManager._instance.plantList.Length)];

                break;
            }
        }

        nextSprout++;

        Time.timeScale = baseTimeScale;
    }

    private void Update()
    {
        Vector3 mousePos;
        mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10));

        tool.transform.position = new Vector3(mousePos.x, mousePos.y, 0);
    }

    public void SetTool(string toolToSet)
    {
        activeTool = (ToolEnum) System.Enum.Parse(typeof(ToolEnum), toolToSet);

        switch (activeTool)
        {
            case ToolEnum.Hand:
                tool.GetComponent<SpriteRenderer>().color = Color.white;
                break;

            case ToolEnum.Shovel:
                tool.GetComponent<SpriteRenderer>().color = Color.grey;
                break;

            case ToolEnum.Water:
                tool.GetComponent<SpriteRenderer>().color = Color.blue;
                break;

            case ToolEnum.Infos:
                tool.GetComponent<SpriteRenderer>().color = Color.cyan;
                break;

            case ToolEnum.Brush:
                tool.GetComponent<SpriteRenderer>().color = Color.yellow;
                break;

            default:
                break;
        }
    }

    public void QuitSubScene()
    {
        if (SceneManager.sceneCount > 1)
            SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(1));
        onFocus = false;

        tool.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void QuitTuto()
    {
        isTuto = false;

        StartCoroutine("UnlockSprout");
    }

    IEnumerator UnlockSprout()
    {
        bool spawned = false;

        foreach(GameObject slot in customerSlots)
        {
            if (!slot.activeSelf)
            {
                slot.SetActive(true);
                var customer = Instantiate(prefabCustomer, slot.transform.position - new Vector3(5, 0, 0), Quaternion.identity, slot.transform);
                customer.GetComponent<Customer>().hisPlant = plantList[Random.Range(0, GameManager._instance.plantList.Length)];

                spawned = true;

                break;
            }
        }

        if (!spawned)
        {
            yield return new WaitForSeconds(5);
        }

        nextSprout++;

        if (nextSprout % 3 == 3)
            Time.timeScale += .5f;

        yield return new WaitForSeconds(20 - Mathf.Sqrt(10f * nextSprout));
        StartCoroutine("UnlockSprout");
    }

    public void GameOver()
    {
        StartCoroutine("LoadGameOver");
    }

    private IEnumerator LoadGameOver()
    {
        yield return new WaitForSecondsRealtime(2f);
        SceneManager.LoadScene("GameOver");
        Debug.LogError("GameOver");
    }
}