﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spraying : MonoBehaviour
{
    public SpriteRenderer pot;

    public Animator dropplets;

    public Animator plicploc;

    float cooldown;

    private void Start()
    {
        pot.sprite = GameManager._instance.plantFocused.sprite.sprite;
    }

    private void Update()
    {
        if (GameManager._instance.plantFocused.waterLevel >= GameManager._instance.plantFocused.plant.waterNeeded)
            dropplets.SetBool("Drops", true);
        else
            dropplets.SetBool("Drops", false);
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && cooldown <= 0)
        {
            if (!GameManager._instance.plantFocused.plant.needSpray)
            {
                GameManager._instance.plantFocused.resistance -= 1;
            }
            GameManager._instance.plantFocused.waterLevel += 1;
            FindObjectOfType<AudioManager>().PlaySound("Pchit");
            plicploc.SetTrigger("Pshit");

            cooldown = .3f;
        }
        else
        {
            cooldown -= Time.deltaTime;
        }
    }

    public void Validation()
    {
        FindObjectOfType<AudioManager>().PlaySound("Pop02");
        GameManager._instance.QuitSubScene();
    }
}
