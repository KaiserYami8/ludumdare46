﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetection : MonoBehaviour
{
    public bool isOverGround;
    public GameObject ground;

    public Vector3 lastPosition;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            isOverGround = true;
            ground = collision.gameObject;
            lastPosition = transform.parent.position;
        }
        else
        {
            isOverGround = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        isOverGround = false;
    }
}