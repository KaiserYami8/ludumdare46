﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PouringDirt : MonoBehaviour
{
    public DirtType[] dirtTypes;

    public Text plantName;
    public Text plantNotes;
    public Text percent;

    public Transform mask;

    public GameObject[] plants;

    GameManager gameManager;

    int total;


    private void Awake()
    {
        gameManager = GameManager._instance;
    }

    private void Update()
    {
        plantName.text = gameManager.plantFocused.plant.plantName;

        plantNotes.text = null;

        foreach(DirtType dirtType in gameManager.plantFocused.plant.dirtNeeded)
        {
            plantNotes.text += "- " + dirtType.dirtType + " : " + dirtType.quantity + "%\n";
        }
    }

    public void Validation()
    {
        FindObjectOfType<AudioManager>().PlaySound("Pop02");
        float diff = 0;
        foreach (DirtType dt in dirtTypes)
        {
            bool found = false;
            foreach (DirtType dtNeeded in gameManager.plantFocused.plant.dirtNeeded)
            {
                if (dt.dirtType == dtNeeded.dirtType)
                {
                    diff += Mathf.Abs(dt.quantity - dtNeeded.quantity);
                    found = true;
                }
            }
            if (!found)
            {
                diff += dt.quantity;
            }
        }

        if (gameManager.plantFocused.isASprout)
        {
            foreach(GameObject pb in plants)
            {
                if (pb.GetComponent<PlantBehaviour>().plant.plantName == gameManager.plantFocused.plant.plantName)
                {
                    pb.GetComponent<PlantBehaviour>().resistance = 100 - diff;
                    Vector3 pos = gameManager.plantFocused.transform.position + new Vector3(0, pb.GetComponent<BoxCollider>().size.y / 2 - pb.GetComponent<BoxCollider>().center.y, 0);
                    Instantiate(pb, pos, gameManager.plantFocused.transform.rotation);
                    Destroy(gameManager.plantFocused.gameObject);

                    break;
                }
            }

            if (gameManager.isTuto)
            {
                GameObject.FindGameObjectWithTag("Arrow").transform.GetChild(0).gameObject.SetActive(false);
                gameManager.QuitTuto();
            }
        }
        else
        {
            gameManager.plantFocused.resistance = 100 - diff;
        }

        gameManager.QuitSubScene();
    }

    public void AddDirt(string type)
    {
        if (total < 100)
        {
            FindObjectOfType<AudioManager>().PlaySound("Soil");

            switch ((DirtType.dirtTypeEnum)System.Enum.Parse(typeof(DirtType.dirtTypeEnum), type))
            {
                case DirtType.dirtTypeEnum.PottingSoil:
                    dirtTypes[0].quantity += 10;
                    break;

                case DirtType.dirtTypeEnum.ClayBall:
                    dirtTypes[1].quantity += 10;
                    break;

                case DirtType.dirtTypeEnum.Sand:
                    dirtTypes[2].quantity += 10;
                    break;

                case DirtType.dirtTypeEnum.Peat:
                    dirtTypes[3].quantity += 10;
                    break;

                case DirtType.dirtTypeEnum.Compost:
                    dirtTypes[4].quantity += 10;
                    break;

                default:
                    break;
            }
            mask.position += new Vector3(0, .5f, 0);
            total += 10;
            percent.text = total + "%";
        }
    }
}

[System.Serializable]
public class DirtType
{
    public enum dirtTypeEnum { PottingSoil, ClayBall, Sand, Peat, Compost }

    public dirtTypeEnum dirtType;
    public float quantity;
}