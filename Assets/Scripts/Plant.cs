﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlantData", menuName = "ScriptableObjects/Plant", order = 1)]
public class Plant : ScriptableObject
{
    public string plantName;

    public Sprite sprite;
    public Sprite dry;
    public Sprite dead;

    [Header("Dirt")]
    public DirtType[] dirtNeeded;
    [Header("Water")]

    public float waterNeeded;
    public float speedDrying;
    public bool needSpray;

    [Header("Light")]
    [Range(0.0f, 1.0f)]
    public float lightNeeded;
    public float heatWeakness = 1;
    public float coldWeakness = 1;
}
